"""
  贪吃蛇小游戏
    游戏显示：游戏界面、结束界面
    贪吃蛇：头部、身体、食物判断、死亡判断
    果实：随机生成
    按键控制：上、下、左、右
"""
import pygame, sys, time, random
import tkinter as tk
import os
from pygame.locals import *

# 需要的宽高数据
screenWidth = tk.Tk().winfo_screenwidth()
screenHeight = tk.Tk().winfo_screenheight()
width = 320
height = 320

# 定义颜色变量
redColour = pygame.Color(255, 0, 0)
blackColour = pygame.Color(0, 0, 0)
whiteColour = pygame.Color(255, 255, 255)
greyColour = pygame.Color(150, 150, 150)


# 初始化可用果实位置表（为解决果实生成位置与身体重合）
def initRaspberryPositionList(width, height):
    # 构建出窗口中所有的点
    list = []
    for w in range(0, width, 20):
        for h in range(0, height, 20):
            list.append([w, h])
    return list


def main():
    # 初始化变量
    snakePosition = [100, 100]  # 贪吃蛇 蛇头的初始位置
    snakeSegments = [[100, 100]]  # 贪吃蛇 蛇的身体，初始为一个单位
    raspberryPosition = [300, 100]  # 果实的初始位置
    raspberrySpawned = 0  # 果实的个数为1
    direction = 'right'  # 初始方向为右
    changeDirection = direction
    score = -1  # 初始得分

    raspberryList = initRaspberryPositionList(width, height)  # 果实可出现位置列表
    # index = raspberryList.index(snakePosition)
    # raspberryList.pop(index)

    # 初始化pygame
    pygame.init()
    fpsClock = pygame.time.Clock()  # 获取时钟对象实例

    # 窗口显示位置（居中显示）
    os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % ((screenWidth-width)/2, (screenHeight-height)/2)

    # 创建pygame显示层
    playSurface = pygame.display.set_mode((width, height))  # 窗口大小
    pygame.display.set_caption('Snake Game')  # 窗口名称

    while True:
        # 检测按键等pygame事件
        for event in pygame.event.get():
            if event.type == QUIT:  # ESC触发QUIT事件
                pygame.quit()
                sys.exit()
            elif event.type == KEYDOWN:  # 判断键盘事件
                if event.key == K_RIGHT or event.key == ord('d'):
                    changeDirection = 'right'
                if event.key == K_LEFT or event.key == ord('a'):
                    changeDirection = 'left'
                if event.key == K_UP or event.key == ord('w'):
                    changeDirection = 'up'
                if event.key == K_DOWN or event.key == ord('s'):
                    changeDirection = 'down'
                if event.key == K_ESCAPE:  # ESC按下
                    pygame.event.post(pygame.event.Event(QUIT))

        # 判断是否输入了反方向
        if changeDirection == 'right' and not direction == 'left':
            direction = changeDirection
        if changeDirection == 'left' and not direction == 'right':
            direction = changeDirection
        if changeDirection == 'up' and not direction == 'down':
            direction = changeDirection
        if changeDirection == 'down' and not direction == 'up':
            direction = changeDirection

        # 根据方向移动蛇头的坐标
        if direction == 'right':
            snakePosition[0] += 20
        if direction == 'left':
            snakePosition[0] -= 20
        if direction == 'up':
            snakePosition[1] -= 20
        if direction == 'down':
            snakePosition[1] += 20

        # 增加蛇的长度
        snakeSegments.insert(0, list(snakePosition))

        # 判断是否吃掉了果实
        if snakePosition[0] == raspberryPosition[0] and snakePosition[1] == raspberryPosition[1]:
            raspberrySpawned = 0
        else:
            raspberryList.append(snakeSegments[-1])
            snakeSegments.pop()

        # 如果吃掉果实，则重新生成果实
        if raspberrySpawned == 0:
            score += 1
            i = random.randrange(-1, width/20 * height/20 - score)
            raspberryPosition = raspberryList[i]
            raspberrySpawned = 1

        # 绘制pygame显示层
        playSurface.fill(blackColour)
        for position in snakeSegments:
            pygame.draw.rect(playSurface, whiteColour, Rect(position[0], position[1], 20, 20))
        pygame.draw.rect(playSurface, redColour, Rect(raspberryPosition[0], raspberryPosition[1], 20, 20))
        # 刷新pygame显示层
        pygame.display.flip()

        # 判断是否死亡
        if snakePosition[0] >= width or snakePosition[0] < 0:
            gameOver(playSurface, score)
        if snakePosition[1] >= height or snakePosition[1] < 0:
            gameOver(playSurface, score)
        # for snakeBody in snakeSegments[1:]:
        #     if snakePosition[0] == snakeBody[0] and snakePosition[1] == snakeBody[1]:
        if snakePosition in snakeSegments[1:]:
            gameOver(playSurface, score)

        i = raspberryList.index(snakePosition)
        raspberryList.pop(i)
        # 控制游戏速度
        # 通过时钟对象指定循环频率（每秒循环5次）
        fpsClock.tick(5)


# 游戏结束
def gameOver(playSurface, score):
    # GameOver显示
    gameOverFont = pygame.font.SysFont('arial.ttf', 54)
    gameOverSurf = gameOverFont.render('Game Over!', True, greyColour)
    gameOverRect = gameOverSurf.get_rect()
    gameOverRect.midtop = (width/2, height/4)  # 矩形中上距窗口左上角
    playSurface.blit(gameOverSurf, gameOverRect)

    # Score显示
    scoreFont = pygame.font.SysFont('arial.ttf', 54)
    scoreSurf = scoreFont.render('Score:' + str(score), True, greyColour)
    scoreRect = scoreSurf.get_rect()
    scoreRect.midtop = (width/2, height/2)
    playSurface.blit(scoreSurf, scoreRect)
    pygame.display.flip()

    # 短暂休眠后退出
    time.sleep(3)
    pygame.quit()
    sys.exit()


if __name__ == "__main__":
    main()
