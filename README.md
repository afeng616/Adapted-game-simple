## 一份属于自己的贪吃蛇代码 ##

***贪吃蛇是Nokia时代经常玩的游戏，对于我来说，实现这些小游戏也有着别样的乐趣！！！***

### 贪吃蛇实例 ###
![实例1.gif](./GIF1.gif)
---------------------------------------------------

### 思路 ###
经过对以往贪吃蛇游戏的回忆，了解到贪吃蛇实现需要注意以下几个问题
* 游戏界面的简单绘制  
* 贪吃蛇的移动
* 果实的随机出现
* 果实出现不可与蛇身重合
* 判断贪吃蛇是否吃到果实
* 判断贪吃蛇是否死亡
* 游戏结束界面的简单绘制

### 准备 ###
>>本机使用环境: python3.5  
>>使用库: pygame、sys、time、random、tkinter、os

### 实现 ###
*以下是对贪食蛇实现的简单说明：*

<br>
Snake Game界面初始化

~~~ python
pygame.init()  # 初始化pygame

# 创建pygame显示层
playSurface = pygame.display.set_mode((width, height))  # 窗口大小
pygame.display.set_caption('Snake Game')  # 窗口名称

~~~
<br>
游戏界面居中显示

~~~ python
# 需要的宽高数据
screenWidth = tk.Tk().winfo_screenwidth()
screenHeight = tk.Tk().winfo_screenheight()
width = 320
height = 320

# 窗口显示位置（居中显示）
os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % ((screenWidth-width)/2, (screenHeight-height)/2)
~~~
<br>
贪吃蛇移动

~~~ python
# 根据方向移动蛇头的坐标
if direction == 'right':
    snakePosition[0] += 20
if direction == 'left':
    snakePosition[0] -= 20
if direction == 'up':
    snakePosition[1] -= 20
if direction == 'down':
    snakePosition[1] += 20
~~~
<br>
果实随机出现


~~~ python
i = random.randrange(-1, width/20 * height/20 - score)
raspberryPosition = raspberryList[i]
~~~
<br>
判断是否得分

~~~ python
# 判断是否吃掉了果实
if snakePosition[0] == raspberryPosition[0] and snakePosition[1] == raspberryPosition[1]:
    raspberrySpawned = 0
if raspberrySpawned == 0:
    score += 1
~~~
<br>
判断是否死亡

~~~ pyhon
if snakePosition[0] >= width or snakePosition[0] < 0:
    gameOver(playSurface, score)
if snakePosition[1] >= height or snakePosition[1] < 0:
    gameOver(playSurface, score)
for snakeBody in snakeSegments[1:]:
    if snakePosition[0] == snakeBody[0] and snakePosition[1] == snakeBody[1]:
        gameOver(playSurface, score)
~~~
<br>
游戏结束

~~~ python
def gameOver(playSurface, score):
    # GameOver显示
    gameOverFont = pygame.font.SysFont('arial.ttf', 54)
    gameOverSurf = gameOverFont.render('Game Over!', True, greyColour)
    gameOverRect = gameOverSurf.get_rect()
    gameOverRect.midtop = (width/2, height/4)  # 矩形中上距窗口左上角
    playSurface.blit(gameOverSurf, gameOverRect)

    # Score显示
    scoreFont = pygame.font.SysFont('arial.ttf', 54)
    scoreSurf = scoreFont.render('Score:' + str(score), True, greyColour)
    scoreRect = scoreSurf.get_rect()
    scoreRect.midtop = (width/2, height/2)
    playSurface.blit(scoreSurf, scoreRect)
    pygame.display.flip()

    # 短暂休眠后退出
    time.sleep(3)
    pygame.quit()
    sys.exit()
~~~
-------------------------------------------------
说明
----------------------------------
*部分源码来自互联网，细节优化源于个人*
<br>
主要优化，随机果实的出现，避免其出现在蛇身，防止蛇身过长时，游戏出现卡顿情况。

实现思路
-------------------------
创建一个列表存储果实可以出现的所有位置，在需要随机产生果实时，从中抽泣一个值，即为果实出现位置。  
<br>
列表变更时需要注意
* 游戏初始化
    * 将蛇头值从表中除去
* 贪吃蛇移动时
    * 未吃到果实
        * 将蛇头移动后的值从表中除去
        * 将蛇尾移动前的值加入表中
    * 吃到果实
        * 将蛇头移动后的值从表中除去
        
为什么不使用以下思路？
* 生成随机果实
    * 判断果实位置是否与蛇身上
        * 是
            * 重新生成
            * 返回再次进行判断，直至否为止
        * 否
            * 确定果实位置
* 绘制

这个算法思路同样可以实现随机生成果实位置，但当蛇身有一定长度后，生成的果实位置会有很大概率在蛇身上。
从而不断进行判断，重生，判断，重生……导致游戏无法正常进行。<br>
使用列表存储可生成的位置，可以看做将果实位置生成时计算需要的时间分配到了每次生成果实的执行中。
这样在每次生成时就不会出现不断重获位置，否定再重获的情况！
    
------------------------------------
*实现贪食蛇小游戏纯属娱乐，有BUG、不足纯属正常，若有更好的算法或建议欢迎联系本人！
<br>！！！copy请注明出处！！！*
